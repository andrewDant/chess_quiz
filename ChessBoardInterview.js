/**
 * This is your takehome technical assessment.
 * Recommend using VSCode as an IDE, and NodeJS as an execution environment while testing your work.
 * 
 * You will be defining ES6 classes to represent a chess board and chess pieces,
 * then using those definitions to answer questions
 * 
 * The "answers" to the questions should be in the form of a function,
 * which when executed will perform the necessary instantiation and logic to arrive at the answer
 * 
 * You are NOT expected to create any kind of user interaction,
 * we should be able to execute your function and see results in the console upon its completion.
 * 
 

 /** 
 * Below are interface definitions for a Knight chess piece and a chess Board
 * implement both classes according to their definitions.
 */


/**
 * @class Knight
 * @description defines a Knight piece in chess
 * @property {number} x - the current x coordinate of the piece
 * @property {number} y - the current y coordinate of the piece 
 */
class Knight {
  /**
   * Constructs an instance of Knight piece with x and y coordinates
   * @param {Number} [x] - optional, defaults to 0
   * @param {Number} [y] - optional, defaults to 0
   */
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  /**
   * deifnition for a coord, an x-y pair represented as an array of length 2
   * @typeDef {Number[]} coord - an array of length two
   * @property coord[0] - x coordinate
   * @property coord[1] - y coordinate
   */

  /**
   * all possible moves the instantiated Knight could take
   * @returns {coord[]} - an array of x,y coordinates representing the possible moves
   * @example a knight at x,y position 0,0 will return an array of x,y coordinates [1,2] [2,1] [-1,2] [-2,1] [2,-1] [1,-2] [-2,-1] [-1,-2]
   */
  availableMoves() {
    let coords = [];

    // NOTE to reviewer: I had a lot of fun trying out some different ways of doing this, I'd love to hear your opinion of this
    // solution vs the others I came up with saved in movesAlt.js 

    // list of possible pairs of distances from current coordinates
    // for example the first pair matches the position x + 1, y + 2
    let moveDistances = [[1, 2],[2, 1],[-1, 2],[-2, 1],[2, -1],[1, -2],[-2, -1],[-1, -2]];

    // add a possible move for each distance pair
    for (let i = 0; i < moveDistances.length; i++) {
      coords.push([this.x + moveDistances[i][0], this.y + moveDistances[i][1]]);
    }
    return coords;
  }
}

/**
 * @class Board
 * @description defines a chess game board
 * @property {Number[]} xValues - array of possible x coordinates, starts with 0
 * @property {Number[]} yValues - array of possible y coordinates, starts with 0
 * @property {Knight[]} pieces - array of pieces currently on the board
 */
class Board {

  /**
   * Constructs an instance of a Board
   * @param {Number} [xLength] - optional, the length of the x dimension of the board, defaults to 1, must be >= 1
   * @param {Number} [yLength] - optional, the length of the y dimension of the board, defaults to 1, must be >= 1
   * @param {Knight[]} [pieces] - optional, an array of pieces initially on the board, defaults to no pieces
   */
  constructor(xLength = 1, yLength = 1, pieces = []) {
    if (xLength < 1 || yLength < 1) {
      throw new Error("Lengths must be >= 1")
    }

    // fill coordinate arrays with numbers 0 to length-1
    let xValues = [];
    for (let i = 0; i < xLength; i++) {
      xValues.push(i);
    }
    this.xValues = xValues;

    let yValues = [];
    for (let i = 0; i < yLength; i++) {
      yValues.push(i);
    }
    this.yValues = yValues;

    this.pieces = pieces;
  }

  /**
   * adds a piece to the game board
   * @param {Knight} piece - the piece to add to the board
   * @returns {Boolean} - true if the piece was successfully added, false if it was not added (for example, if it already exists on the board)
   */
  addPiece(piece) {
    // check if the piece is in the array or if another piece in the array is already at its position
    for (let i = 0; i < this.pieces.length; i++) {
      let curPiece = this.pieces[i];
      if (curPiece == piece) {
        // piece already in array
        return false;
      } else if (curPiece.x == piece.x && curPiece.y == piece.y) {
        // another piece occupies that spot
        return false;
      }
    }
    // otherwise add the piece to the board 
    this.pieces.push(piece);
    return true;
  }

  /**
   * removes a piece from the game board
   * @param {Knight} piece - the piece to remove from the board
   * @returns {Knight|false} - if the piece was removed, return the piece, otherwise return false if unable to remove the piece from the board (for example, if the piece is not currently on the board) 
   */
  removePiece(piece) {
    // find the index of the piece if it's in the array, else -1
    let index = this.pieces.indexOf(piece);
    if (index >= 0) {
      // remove the piece from its index in the array of pieces and return it
      return this.pieces.splice(index, 1);
    } else {
      // if the piece isn't on the board it can't be removed
      return false;
    }
  }

  /**
   * determines whether the given piece is allowed to make the requested move
   * @param {Knight} piece - the piece that will attempt to move
   * @param {Number} xCoord - the xCoordinate the piece wants to move to
   * @param {Number} yCoord - the yCoordinate the piece wants to move to
   * @returns {Boolean} - true if the requested move is valid for this board, false otherwise
   */
  validateMove(piece, xCoord, yCoord) {
    // if the piece isn't on the board no move is valid
    if (!this.pieces.includes(piece)) {
      return false;
    }
    // move isn't valid if xCoord is outside valid range
    if (xCoord < 0 || xCoord >= this.xValues.length) {
      return false;
    }
    // move isn't valid if yCoord is outside valid range
    if (yCoord < 0 || yCoord >= this.yValues.length) {
      return false;
    }

    return true;
  }

  /**
   * moves a piece on the board, and if the move results in taking a piece, removes the taken piece
   * @param {Knight} piece - the piece that will make a move
   * @param {Number} xCoord - the xCoordinate the piece is moving to
   * @param {Number} yCoord - the yCoordinate the piece is moving to
   * @returns {Knight|Boolean} - false if the piece failed to complete a move, true if the piece moved to an empty space, or the removed piece if the move results in taking a piece.
   */
  executeMove(piece, xCoord, yCoord) {
    // check if the move is valid, if not return false
    if (!this.validateMove(piece, xCoord, yCoord)) {
      return false;
    }
    // move the piece
    piece.x = xCoord;
    piece.y = yCoord;

    // check if any other piece was at that position
    for (let i = 0; i < this.pieces.length; i++) {
      let curPiece = this.pieces[i];
      if (curPiece != piece && curPiece.x == xCoord && curPiece.y == yCoord) {
        // found a piece to remove
        return this.pieces.splice(i, 1);
      }
    }
    // didn't find a piece in that spot so it must be empty
    return true;
  }

}


/**
 * QUESTION ONE
 * Create a 4x4 Board and 4 Knight Pieces
 * Place 3 of the Knight Pieces at x=0 y=1, x=2 y=2, and x=3 y=1
 * Place the 4th Knight at x=2 y=0
 * create a function that determines the fewest number of moves (less than 15) of the 4th Knight to capture the first 3 knights
 * capturing occurs when a piece makes a valid move to an already-occupied square
 * log to the console the fewest number of moves, and the x-y coordinates for each move the 4th Knight takes to perform the captures.
 */
function QuestionOne() {
  // create 4x4 board
  let board = new Board(4, 4);

  // create 4 knights at specified positions
  let k1 = new Knight(0, 1);
  let k2 = new Knight(2, 2);
  let k3 = new Knight(3, 1);
  let k4 = new Knight(2, 0);

  // add each knight to board
  board.addPiece(k1);
  board.addPiece(k2);
  board.addPiece(k3);
  board.addPiece(k4);

  // print number of moves then the x-y coords for each move
  let path = shortestElimination(board, k4);
  console.log("Question One");
  console.log("Number of moves: " + path.length);
  path.forEach(coord => {
    console.log("X: " + coord[0] + " Y: " + coord[1]);
  });

  // print blank line for spacing
  console.log();
}

/**
   * determines the shortest path for a given piece to eliminate all specified pieces on the board
   * @param {Board} board - the board on which to find the path
   * @param {Knight} piece - the piece that will make moves to eliminate others
   * @returns {coord[]} - array of x-y coords for each move the piece takes to perform the captures
   */
function shortestElimination(board, piece) {
  let pathList = [];
  let path = [];
  
  // fills in pathList with all found paths
  shortestRecurse(board, piece, path, pathList);

  // find which of the paths has the smallest length
  let shortestIndex = 0;
  for (let i = 0; i < pathList.length; i++) {
    const curPath = pathList[i];
    if (curPath.length < pathList[shortestIndex].length) {
      shortestIndex = i;
    }
  }
  return pathList[shortestIndex];
}

/**
   * recursive helper for determining shortest path. Adds all valid paths to pathList
   * @param {Board} board - the board on which to find the path
   * @param {Knight} piece - the piece that will make moves to eliminate others
   * @param {coord[]} path - array of x-y coords for each move the piece takes to perform the captures
   * @param {coord[][]} pathList - array of all valid paths found so far
   */
function shortestRecurse(board, piece, path, pathList) {
  // NOTE: according to project description max length should probably be 15, but setting it lower than that
  //        makes a significant difference in run time and reduces the number of method calls by a few million

  // determines the maximum length a path can reach before being discarded
  const MAX_LENGTH = 10;

  // find all possible next positions for the piece
  let moveOptions = piece.availableMoves();

  moveOptions.forEach(newCoord => {
    // need a copy of all params so calls don't affect state of other calls
    let newPath = path.slice();
    let newPiece = new Knight(piece.x, piece.y);
    let newBoard = new Board(4,4, board.pieces.slice());
    newBoard.removePiece(piece);
    newBoard.addPiece(newPiece);
    
    // check the result of executing each possible change
    let result = newBoard.executeMove(newPiece, newCoord[0], newCoord[1]);
    // if change is valid and newPath isn't too long
    if (result && newPath.length < MAX_LENGTH) {
      // add new position to newPath
      newPath.push(newCoord);
      if(newBoard.pieces.length == 1) {
        // if only one piece is left a valid path was found
        pathList.push(newPath);
      } else {
        // otherwise recurse until a base condition
        shortestRecurse(newBoard, newPiece, newPath, pathList);
      }
    }
  });
}

/**
 * QUESTION TWO
 * Create a 4 x 4 Board and a Knight Piece
 * Place the Knight at coordinates x=2 y=0
 * create a function that determines a series of moves for the knight to touch every square on the board at least once
 * Log to the console the set of moves taken as an array of x-y coordinates
 */
function QuestionTwo() {
  // create 4x4 board
  let board = new Board(4, 4);

  // create the knight at specified position
  let k1 = new Knight(2, 0);
  // add the knight to board
  board.addPiece(k1);

  // TODO skip first position?

  // print the x-y coords for each position
  let path = everySpotPath(board, k1);
  console.log("Question Two");
  path.forEach(coord => {
    console.log("X: " + coord[0] + " Y: " + coord[1]);
  });
}

function everySpotPath(board, piece) {
  // create a list of every spot the piece should hit (aka every spot)
  let SPOT_LIST = [];
  for(let i = 0; i < board.xValues.length; i++) {
    for(let j = 0; j < board.yValues.length; j++) {
      SPOT_LIST.push([i,j]);
    }
  }
  // initialize path to include starting position
  let path = [[piece.x, piece.y]];
  return everySpotRecurse(board, piece, path, SPOT_LIST);
}

// TODO: seems to be an issue with my current solution
// but I figured out you can get to any one spot on the board to any other spot in 4 moves (maybe 5?)
// so make a function to find shortest path from one spot to another, make a visited matrix same size as the board
// go through spots on the board visiting one at a time and marking any spots you hit on the way as visited
// every time you reach a destination choose a new (unvisited) destination. Gives a reasonable maximum amount of work and moves

function everySpotRecurse(board, piece, path, SPOT_LIST) {
  // determines the maximum length a path can reach before being discarded
  const MAX_LENGTH = 25;

  // find all possible next positions for the piece
  let moveOptions = piece.availableMoves();
  
  for (const newCoord of moveOptions) {
    // need a copy of all params so calls don't affect state of other calls
    let newPath = path.slice();
    let newPiece = new Knight(piece.x, piece.y);
    let newBoard = new Board(4,4, board.pieces.slice());
    newBoard.removePiece(piece);
    newBoard.addPiece(newPiece);
    
    // check the result of executing each possible change
    let result = newBoard.executeMove(newPiece, newCoord[0], newCoord[1]);
    // if change is valid and newPath isn't too long
    if (result && newPath.length < MAX_LENGTH) {
      // add new position to newPath
      newPath.push(newCoord);

      // TODO not the right condition

      if(SPOT_LIST.every(function(val) { return newPath.indexOf(val) >= 0; })) {
        // the path is length 16 with no repeats it is done
        return newPath;
      } else {
        // otherwise recurse until a base condition
        newPath = everySpotRecurse(newBoard, newPiece, newPath, SPOT_LIST);
        if(newPath) {
          return newPath;
        }
      }
    }
  }
  // couldn't find a solution from this position
  return false;
}

QuestionOne();
QuestionTwo();



/**
 * BONUS 1
 * Redefine the above classes such that there is a Piece class that the Knight class extends
 * Then, create another piece of your choice by extending the Piece class
 */

/**
 * BONUS 2
 * Enance the pieces to contain a new property 'color', which can be either white or black
 * Update the class definitions such that a piece cannot make a move that would result in capturing another piece of the same color.
 */

/**
 * BONUS 3
 * Redo QUESTION ONE, where the knights at x=2 y=2 and x=2 y=0 are black,
 * the other two are white, and your 4th (black) night must capture the two white knights in the fewest number of moves
 */