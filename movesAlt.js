/**
 * all possible moves the instantiated Knight could take
 * @returns {coord[]} - an array of x,y coordinates representing the possible moves
 * @example a knight at x,y position 0,0 will return an array of x,y coordinates [1,2] [2,1] [-1,2] [-2,1] [2,-1] [1,-2] [-2,-1] [-1,-2]
 */
availableMoves() {
  var coords = [];

  // for each combination of {x +/- 1 and y +/- 2} or {x +/- 2 and y +/- 2}
  for (var i = -2; i <= 2; i++) {
    for (var j = -2; j <= 2; j++) {
      if (i != 0 && j != 0 && Math.abs(i) != Math.abs(j)) {
        coords.push([this.x + i, this.y + j]);
      }
    }
  }

  // for each possible change in x determine the two possible changes in y
  for (var i = -2; i <= 2; i++) {
    if (i != 0) {
      // j is 2 if i is 1 and vice versa
      var j = (i % 2) + 1;
      // add for + and - j
      coords.push([this.x + i, this.y + j]);
      coords.push([this.x + i, this.y - j]);
    }
  }

  var xDists = [ 2, 1, -1, -2, -2, -1, 1, 2 ];
  var yDists = [ 1, 2, 2, 1, -1, -2, -2, -1 ];

  for (var i = 0; i < xDists.length; i++) {
    coords.push([this.x + xDists[i], this.y + yDists[i]]);
  }

  var moveDistances = [[1,2] [2,1] [-1,2] [-2,1] [2,-1] [1,-2] [-2,-1] [-1,-2]];
  for (var i = 0; i < moveDistances.length; i++) {
    coords.push([this.x + moveDistances[i][0], this.y + moveDistances[i][1]]);
  }

  return coords;
}